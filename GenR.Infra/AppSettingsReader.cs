﻿using System;
using System.Configuration;
using System.Linq;

namespace GenR.Infra
{
    /// <summary>
    /// Simple helper function to read appsettings
    /// </summary>
    public class AppSettingsReader
    {
        /// <summary>
        /// Return the value of a given key in the appsettings. The generic Type T should always be a primitive. In case of doubt use string for T
        /// </summary>
        /// <param name="key"></param>
        /// <param name="throwOnError">Defaults to true. If set to false and an error occurs while reading app settings then default(T) is returned</param>1
        /// <returns></returns>
        public static T ValueAs<T>(string key, bool throwOnError = true)
        {
            try
            {
                var keyExists = ConfigurationManager.AppSettings.HasKeys() && ConfigurationManager.AppSettings.AllKeys.Contains(key);
                if (!keyExists) throw new Exception(key + " not found in app settings");
                var result = ConfigurationManager.AppSettings.GetValues(key).FirstOrDefault();
                if (string.IsNullOrWhiteSpace(result)) throw new Exception("Value for " + key + " is empty");

                return (T)Convert.ChangeType(result, typeof(T));
            }
            catch
            {
                if (throwOnError)
                    throw; //There is an error in web config so just throw. The error needs to be fixed before proceeding.
                else
                    return default(T);
            }
        }
    }
}