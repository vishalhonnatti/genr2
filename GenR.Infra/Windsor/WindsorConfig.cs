﻿using System.Web.Http;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using GenR.Domain;
using GenR.Infra.Mongo;

namespace GenR.Infra.Windsor
{
    public static class WindsorConfig
    {
        public static IWindsorContainer Container;

        private static IWindsorContainer configureContainer()
        {
            var container = new WindsorContainer();
            container.Register(
                Component.For(typeof(IRepository<>))
                    .ImplementedBy(typeof(MongoRepository<>))
                        .DependsOn(new { connectionString = AppSettingsReader.ValueAs<string>("Mongo.URL") })
            );

            container.Register(
                   AllTypes.FromAssemblyNamed("GenR.Services")
                   .BasedOn<IService>()
                   .If(t => t.Name.Contains("Service"))
                   .LifestyleTransient()
           );

            container.Register(
                    AllTypes.FromAssemblyNamed("GenR.API")
                    .BasedOn<ApiController>()
                    .If(t => t.Name.EndsWith("Controller"))
                    .LifestyleTransient()
            );
            return container;
        }
        /// <summary>
        /// Configures the container and skips dependency resolver. Use in unit testing.
        /// </summary>
        public static void Configure()
        {
            Container = configureContainer();
        }

        /// <summary>
        /// Configures the container and sets the dependency resolver.
        /// </summary>
        /// <param name="config"></param>
        public static void Configure(HttpConfiguration config)
        {
            WindsorConfig.Configure();//Configure the container first
            config.DependencyResolver = new WindsorResolver(Container);
            ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(Container));
        }
    }
}