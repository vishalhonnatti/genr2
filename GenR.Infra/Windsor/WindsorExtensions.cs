﻿using System;
using Castle.Windsor;

namespace GenR.Infra.Windsor
{
    public static class WindsorExtensions
    {
        public static T Build<T>(this IWindsorContainer container, Type type)
        {
            return (T)container.Resolve(type);
        }
    }
}