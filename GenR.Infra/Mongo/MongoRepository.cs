﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GenR.Domain;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace GenR.Infra.Mongo
{
    public class MongoRepository<T> : IRepository<T> where T : Entity
    {
        public MongoCollection<T> Collection { get; private set; }
        public MongoDatabase DB { get; private set; }

        public MongoRepository(string connectionString)
        {
            //var connectionString = MongoDBSettings.ConnectionString;
            var mongoUrl = new MongoUrl(connectionString);
            DB = MongoDatabase.Create(mongoUrl);
            var collectionName = ((Entity)Activator.CreateInstance(typeof(T), true)).CollectionName;
            Collection = DB.GetCollection<T>(collectionName);
        }

        public T GetById(string id)
        {
            try
            {
                return Collection.AsQueryable().Where(r => r.Id == new ObjectId(id)).FirstOrDefault();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Returns the T (1 record) by the given criteria
        /// </summary>
        /// <param name="criteria">The expression</param>
        /// <returns>The T</returns>
        public T GetSingle(Expression<Func<T, bool>> criteria)
        {
            return Collection.AsQueryable<T>().Where(criteria).FirstOrDefault();
        }

        /// <summary>
        /// Retunrs the list of T where it matches the criteria
        /// </summary>
        /// <param name="criteria">The expression</param>
        /// <returns>List of T</returns>
        public IQueryable<T> GetAll(Expression<Func<T, bool>> criteria)
        {
            return Collection.AsQueryable().Where(criteria);
        }

        /// <summary>
        /// Retunrs the All the records of T
        /// </summary>
        /// <returns>List of T</returns>
        public IQueryable<T> GetAll()
        {
            return Collection.AsQueryable();
        }

        public PagedResult<T> GetPagedData(int pageIndex, int pageSize, string sortOrder, string orderBy = "asc")
        {
            MongoCursor<T> mc = Collection.FindAllAs<T>();
            mc = sortOrder == "asc" ? mc.SetSortOrder(SortBy.Ascending(orderBy)) : mc.SetSortOrder(SortBy.Descending(orderBy));
            mc.SetSkip(pageIndex * pageSize);
            mc.SetLimit(pageSize);
            PagedResult<T> result = new PagedResult<T>();
            result.PageIndex = pageIndex;
            result.PageSize = pageSize;
            result.TotalRecords = mc.Count();
            List<T> list = new List<T>();
            list.AddRange(mc.ToList());
            result.PageData = list.AsEnumerable();

            return result;
        }

        public T Add(T item)
        {
            Collection.Insert<T>(item);

            return item;
        }

        public T Update(T item)
        {
            Collection.Save<T>(item);

            return item;
        }

        public void Delete(string idstring)
        {
            //Collection.Remove(Query.EQ("_id", objectId));
            var id = ObjectId.Parse(idstring);
            Collection.Remove(Query.EQ("_id", id));
        }

        /// <summary>
        /// Counts the total records saved in db.
        /// </summary>
        /// <returns>Int value</returns>
        public int Count()
        {
            return (int)Collection.Count();
        }

        /// <summary>
        /// Checks if the entity exists for given criteria
        /// </summary>
        /// <typeparam name="T">The T</typeparam>
        /// <param name="criteria">The expression</param>
        /// <returns>true or false</returns>
        public bool Exists(Expression<Func<T, bool>> criteria)
        {
            return Collection.AsQueryable().Any(criteria);
        }

        /// <summary>
        /// Returns an IQueryable for the given entity
        /// </summary>
        /// <returns>The IQueryable </returns>
        public IQueryable<T> AsQueryable()
        {
            return Collection.AsQueryable();
        }

        public override string ToString()
        {
            return string.Format("DB : {0}, Collection {1}", DB.Name, Collection.Name);
        }

    }
}