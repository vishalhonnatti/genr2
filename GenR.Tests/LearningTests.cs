﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GenR.Infra.Mongo;

namespace GenR.Tests
{
    [TestFixture]
    public class LearningTests
    {
        [Test]
        public void show_typeof_mongo_repository()
        {
            var x = typeof(MongoRepository<>);
            Console.WriteLine(x);
        }

        [Test]
        public void show_typeof_string()
        {
            var x = "test123";
            Console.WriteLine(x.GetType().ToString());
        }
    }
}
