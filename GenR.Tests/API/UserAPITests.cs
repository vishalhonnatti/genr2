﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Castle.Windsor;
using GenR.Infra.Windsor;
using System.Net.Http;
using Newtonsoft.Json;
using GenR.Domain.Security;
using NLog;
using System.Net;

namespace GenR.Tests.API
{
    [TestFixture]
    public class UserAPITests
    {
        private string apiURL = "http://localhost:4949/api/";
        HttpClient client = new HttpClient();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [TestFixtureSetUp]
        public void Init()
        {
            client.BaseAddress = new Uri(apiURL);
            client.DefaultRequestHeaders.Add("APIKEY", "test");
        }

       
        [Test]
        public void can_get_all()
        {
            Console.WriteLine(apiURL + "user");
            Console.WriteLine(new string('-', 80));
            var task = client.GetAsync("user");
            var response = task.Result;
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<dynamic>().Result;
            Console.WriteLine(result);
        }
        [Test]
        public void can_apply_top()
        {
            var task = client.GetAsync("user?$top=2&$skip=0");//&$filter=Status eq 'Disabled'
            Console.WriteLine(apiURL + "user?$top=2&$skip=0");//&$filter=Status eq 'Disabled'
            Console.WriteLine(new string('-', 80));
            var response = task.Result;
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<dynamic>().Result;
            Console.WriteLine(result);
        }
        [Test]
        public void can_paginate_with_top_and_skip()
        {
            var inlinecount = 20;
            var pageSize = 1;
            var pages = Math.Ceiling((double)inlinecount / pageSize);
            for (int i = 1; i <= pages; i++)
            {
                var currentPage = i.ToString();
                var skipVal = pageSize * (i - 1);
                var resource = "user";
                var queryString = string.Format("?$top={0}&$skip={1}", pageSize, skipVal);
                var url = apiURL + resource + queryString;
                Console.WriteLine("Now getting page:" + currentPage + " of " + pages + " - " + url);
                Console.WriteLine(new string('-', 80));
                var task = client.GetAsync(url);
                var response = task.Result;
                response.EnsureSuccessStatusCode();
                var result = response.Content.ReadAsAsync<dynamic>().Result;
                Console.WriteLine(result);
            }
        }
        [Test]
        public void can_get_all_perms()
        {
            Console.WriteLine(apiURL + "perm");
            Console.WriteLine(new string('-', 80));
            var task = client.GetAsync("perm");
            var response = task.Result;
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<dynamic>().Result;
            Console.WriteLine(result);
        }
    }
}
