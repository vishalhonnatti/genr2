﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Net.Http;
using NLog;
using System.Net;

namespace GenR.Tests.API
{
    public class PipelineTests
    {
        [TestFixture]
        public class APIKeyHandler
        {
            private string apiURL = "http://localhost:4949/api/";
            HttpClient client = new HttpClient();
            private static Logger logger = LogManager.GetCurrentClassLogger();
            private static string APIKEY = "7da4389cbe28";
            [TestFixtureSetUp]
            public void Init()
            {
                client.BaseAddress = new Uri(apiURL);
            }

            [Test]
            public void cannot_request_without_apikey()
            {
                client.DefaultRequestHeaders.Remove("APIKEY");
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.That(response.StatusCode == HttpStatusCode.Forbidden);
            }
            [Test]
            public void cannot_request_with_empty_apikey()
            {
                client.DefaultRequestHeaders.Remove("APIKEY");
                client.DefaultRequestHeaders.Add("APIKEY", "");
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.That(response.StatusCode == HttpStatusCode.Forbidden);
            }
            [Test]
            public void cannot_request_with_invalid_apikey()
            {
                client.DefaultRequestHeaders.Add("APIKEY", "InvalidKey");
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.That(response.StatusCode == HttpStatusCode.Forbidden);
            }
            [Test]
            public void can_request_with_valid_apikey()
            {
                client.DefaultRequestHeaders.Remove("APIKEY");
                client.DefaultRequestHeaders.Add("APIKEY", APIKEY);
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.That(response.StatusCode == HttpStatusCode.OK);
            }

        }

        [TestFixture]
        public class APITokenHandler
        {
            private string apiURL = "http://localhost:4949/api/";
            HttpClient client = new HttpClient();
            private static Logger logger = LogManager.GetCurrentClassLogger();
            private static string APIKEY = "7da4389cbe28";
            [TestFixtureSetUp]
            public void Init()
            {
                client.BaseAddress = new Uri(apiURL);
            }

            [Test]
            public void can_authenticate_with_valid_credentials()
            {
                client.DefaultRequestHeaders.Add("APIKEY", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "sgmayuri@gmail.com");
                client.DefaultRequestHeaders.Add("Password", "winter123");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.That(response.StatusCode == HttpStatusCode.OK);
            }
            [Test]
            public void authentication_fails_with_empty_userName()
            {
                client.DefaultRequestHeaders.Add("APIKEY", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "");
                client.DefaultRequestHeaders.Add("Password", "winter123");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.That(response.StatusCode == HttpStatusCode.Forbidden);
            }
            [Test]
            public void authentication_fails_with_empty_password()
            {
                client.DefaultRequestHeaders.Add("APIKEY", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "sgmayuri@gmail.com");
                client.DefaultRequestHeaders.Add("Password", "");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.That(response.StatusCode == HttpStatusCode.Forbidden);
            }
            [Test]
            public void authentication_fails_with_invalid_credentials()
            {
                client.DefaultRequestHeaders.Add("APIKEY", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "sgmayuri");
                client.DefaultRequestHeaders.Add("Password", "123");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.That(response.StatusCode == HttpStatusCode.Forbidden);
            }
        }
    }
}
