﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Net.Http;

namespace GenR.Tests.API
{
    [TestFixture]
    public class RoleTests
    {
        private string apiURL = "http://localhost:4949/api/";
        HttpClient client = new HttpClient();
        [TestFixtureSetUp]
        public void Init()
        {
            client.BaseAddress = new Uri(apiURL);
            client.DefaultRequestHeaders.Add("APIKEY", "test");

        }
        [Test]
        public void can_get_perms()
        {
            //50e6b05e4673c3196c8f15fb => admin
            var task = client.GetAsync("role/perms/50e6b0644673c3196c8f15fc");
            var response = task.Result;
            Console.WriteLine(response.RequestMessage.RequestUri.ToString());
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<dynamic>().Result;
            Console.WriteLine(result);
        }
    }
}
