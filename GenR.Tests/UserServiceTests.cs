﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Castle.Windsor;
using GenR.Infra.Windsor;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.Services;
using Newtonsoft.Json;
using NLog;

namespace GenR.Tests
{
    [TestFixture]
    public class UserServiceTests
    {
        public static IWindsorContainer Container;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [TestFixtureSetUp]
        public void Init()
        {
            WindsorConfig.Configure();
            Container = WindsorConfig.Container;
        }

        [TestFixtureTearDown]
        public void Dispose() { }
        
        [Test]
        [ExpectedException(typeof(Exception), ExpectedMessage = "User repo cannot be null.")]
        public void create_user_throws_when_repo_is_null()
        {
            var userService = new UserService(null,null);
            string pwd = string.Empty;
            var user = userService.CreateUser("test", "abc@test.com", out pwd);
            var serObj = JsonConvert.SerializeObject(user);
            Console.WriteLine(serObj);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException), MatchType = MessageMatch.Contains, ExpectedMessage = "Value cannot be null.")]
        public void create_user_throws_when_userName_param_is_null()
        {
            var userService = Container.Resolve<UserService>();
            string pwd = string.Empty;
            var user = userService.CreateUser( string.Empty, "abc@test.com", out pwd);
            var serObj = JsonConvert.SerializeObject(user);
            Console.WriteLine(serObj);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException), MatchType = MessageMatch.Contains, ExpectedMessage = "Value cannot be null.")]
        public void create_user_throws_when_email_param_is_null()
        {
            var userService = Container.Resolve<UserService>();
            string pwd = string.Empty;
            var user = userService.CreateUser( "test", string.Empty, out pwd);
            var serObj = JsonConvert.SerializeObject(user);
            Console.WriteLine(serObj);
        }

        [Test]
        [ExpectedException(typeof(Exception), MatchType = MessageMatch.Contains, ExpectedMessage = "Invalid email.")]
        public void create_user_throws_when_email_is_invalid()
        {
            var userService = Container.Resolve<UserService>();
            string pwd = string.Empty;
            var user = userService.CreateUser("test", "abc123", out pwd);
            var serObj = JsonConvert.SerializeObject(user);
            Console.WriteLine(serObj);
            }
          

    }

}
