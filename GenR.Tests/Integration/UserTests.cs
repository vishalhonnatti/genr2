﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.ComponentModel;
using GenR.Domain;
using GenR.Domain.Security;
using Newtonsoft.Json;
using Castle.Windsor;
using NLog;
using GenR.Infra.Windsor;
using GenR.Services;

namespace GenR.Tests.Integration
{
    [TestFixture]
    public class UserTests
    {
        public static IWindsorContainer Container;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [TestFixtureSetUp]
        public void Init()
        {
            WindsorConfig.Configure();
            Container = WindsorConfig.Container;
        }

        [Test]
        public void can_create_user()
        {
            var userService = Container.Resolve<UserService>();
            string pwd = string.Empty;
            var user = userService.CreateUser("mayuri", "sgmayuri@gmail.com", out pwd);
            var serObj = JsonConvert.SerializeObject(user, Formatting.Indented);
            Console.WriteLine(serObj);
            logger.Info("User {0} created with email {1}, password {2}", user.Name, user.Email, pwd);
        }

        [Test]
        public void can_create_role()
        {
            var roleService = Container.Resolve<AccountService>();
            string pwd = string.Empty;
            var role = roleService.CreateRole("role");
            var serObj = JsonConvert.SerializeObject(role, Formatting.Indented);
            Console.WriteLine(serObj);
            logger.Info("Role created with roleName {0}", role.RoleName);
        }

        [Test]
        public void can_create_perm()
        {
            var permService = Container.Resolve<PermService>();
            var perm = permService.CreatePerm("read");
            var serObj = JsonConvert.SerializeObject(perm, Formatting.Indented);
            Console.WriteLine(serObj);
            logger.Info("Permission created with permName {0}", perm.PermName);
        }

        }
    }

