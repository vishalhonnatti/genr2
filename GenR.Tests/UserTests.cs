﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GenR.Domain.Security;

namespace GenR.Tests
{
    [TestFixture]
    public class UserTests
    {
        [Test]
        public void page_size_is_10_when_user_is_created()
        {
            var user = new User();
            Assert.That(user.Settings != null);
            Assert.That(user.Settings.PageSize.Equals(10));
        }
    }
}
