﻿using System;
using NUnit.Framework;
using System.ComponentModel;
using GenR.Domain;
using GenR.Domain.Security;
using Newtonsoft.Json;
using Castle.Windsor;
using NLog;
using GenR.Infra.Windsor;
using GenR.Services;


namespace GenR.Tests
{
    /// <summary>
    /// Authentication / Authorization tests
    /// </summary>
    [TestFixture]
    public class AuthTests
    {

        public static IWindsorContainer Container;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        [TestFixtureSetUp]
        public void Init()
        {
            WindsorConfig.Configure();
            Container = WindsorConfig.Container;
        }

        [TestFixtureTearDown]
        public void Dispose() { }

        [Test]
        public void user_can_login_with_valid_creds()
        {
            var auth = Container.Resolve<AuthService>();
            var loginStatus = auth.Login("test8", "f5bc4e93");
            var serObj = JsonConvert.SerializeObject(loginStatus, Formatting.Indented);
            Console.WriteLine(serObj);
        }
        [Test]
        public void can_disable_user()
        {
            var repo = Container.Resolve<IRepository<User>>();
            var auth = new AuthService(repo);
            auth.DisableUser("50aa093a4673c301888b989e");
        }
        [Test]
        [ExpectedException(typeof(Exception), MatchType = MessageMatch.Contains, ExpectedMessage = "is disabled.")]
        public void disabled_user_cannot_login()
        {
            var repo = Container.Resolve<IRepository<User>>();
            var auth = new AuthService(repo);
            var loginStatus = auth.Login("test4", "e9e6647f");
        }

        [Test]
        public void can_enable_user()
        {
            var repo = Container.Resolve<IRepository<User>>();
            var auth = new AuthService(repo);
            auth.EnableUser("50b4a0c94673c30560ef4b06");
        }
        [Test]


        public void enabled_user_can_login()
        {
            var repo = Container.Resolve<IRepository<User>>();
            var auth = new AuthService(repo);
            var loginStatus = auth.Login("test4", "7283fb2f");
        }

        [Test]
        public void user_can_reset_password()
        {
            var repo = Container.Resolve<IRepository<User>>();
            var auth = new AuthService(repo);
            auth.ResetPasswordByUser("EX12@test.com");
        }
        [Test]
        public void Change_password_by_user()
        {
            var repo = Container.Resolve<IRepository<User>>();
            var auth = new AuthService(repo);
            auth.ChangePasswordByUser("50ebaeee4673c30f58b8acce", "5ddca112", "winter123");
        }
        [Test]
        public void Admin_reset()
        {
            var repo = Container.Resolve<IRepository<User>>();
            var auth = new AuthService(repo);
            auth.AdminReset("50c6d8334673c3194c437d0d");
        }
        [Test]
        public void can_get_salted_hash()
        {
            var salt = "1c89b69e62d9";
            var pwd = "1234564567";
            var result = SaltedHash.Generate(pwd, salt);
            Console.WriteLine("P: {0}, S: {1}, H: {2}", pwd, salt, result);
        }

    }
}

