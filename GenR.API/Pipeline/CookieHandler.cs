﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using GenR.API.Helpers;

namespace GenR.API.Pipeline
{
    public class CookieHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var ck = request.Headers.GetCookies().FirstOrDefault();
            if (ck != null && ck.Cookies != null)
            {
                var cookie = ck.Cookies.FirstOrDefault(o => o.Name == CookieHelper.AuthCookie);
                if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
                {
                    var loggedInUserId = CookieHelper.GetCookieValue(cookie);
                }
            }
            return base.SendAsync(request, cancellationToken);
        }
    }
}