﻿using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using GenR.API.Helpers;
using GenR.Domain.Security;
using GenR.Infra.Windsor;
using GenR.Services;
using NLog;
using System.Net;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Security.Cryptography;
using GenR.API.Security;
using GenR.Infra;
using GenR.Domain;

namespace GenR.API.Pipeline
{
    public class APITokenHandler : DelegatingHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //logger.Debug("Entering ... " + this.GetType().Name);
            APIToken token = null;

            //Check if the request is for authentication
            var reqForAuthentication = request.RequestUri.ToString().ToLower().EndsWith("/authenticate");
            //retrieve username & password from header

            if (reqForAuthentication)
            {
                var userNameHeader = request.Headers.FirstOrDefault(o => o.Key == Constants.HEADER_USERNAME);
                var passwordHeader = request.Headers.FirstOrDefault(o => o.Key == Constants.HEADER_PASSWORD);
                var userName = userNameHeader.Value.ElementAtOrDefault(0);
                var password = passwordHeader.Value.ElementAtOrDefault(0);
                if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                    return this.SendForbiddenResponse();
                else
                {
                    //authenticate user credentials
                    var user = getUserFromCreds(userName, password);
                    //if invalid send forbidden response
                    if (user == null) return this.SendForbiddenResponse();
                    else //valid 
                    {
                        //prepare encrypted token.
                        var min = AppSettingsReader.ValueAs<int>(Constants.HEADER_TOKEN_EXPIRY_IN_MINS, false);
                        min = min == 0 ? 30 : min;
                        token = new APIToken().ForUser(user.IdString).ExpiresIn(min);
                        var encTokenString = Crypto.Encrypt<APIToken>(token);

                        //send ok response with APIToken header
                        var response = new HttpResponseMessage(HttpStatusCode.OK);
                        response.Headers.Add(Constants.HEADER_APITOKEN, encTokenString);
                        var tsc = new TaskCompletionSource<HttpResponseMessage>();// Note: TaskCompletionSource creates a task that does not contain a delegate.
                        tsc.SetResult(response);
                        return tsc.Task;
                    }
                }
            }
            else //regular request
            {

                //check if APIToken is present
                var apiTokenHeader = request.Headers.FirstOrDefault(o => o.Key == Constants.HEADER_APITOKEN);
                //Check if we have a APIKEY Header with a valid value.

                if (apiTokenHeader.Key == null) return this.SendForbiddenResponse();

                else if (apiTokenHeader.Value == null || apiTokenHeader.Value.Count() < 1 || string.IsNullOrEmpty(apiTokenHeader.Value.ElementAt(0)))
                    return this.SendForbiddenResponse();
                //Validate token
                try
                {
                    token = Crypto.Decrypt<APIToken>(apiTokenHeader.Value.ElementAt(0));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
                if (token == null) return this.SendForbiddenResponse();
                logger.Debug(JsonConvert.SerializeObject(token));
                //TBD: write unit test for expiration
                if (token.HasExpired())
                    return this.SendForbiddenResponse();
                else
                {
                    var min = AppSettingsReader.ValueAs<int>(Constants.HEADER_TOKEN_EXPIRY_IN_MINS, false);
                    min = min == 0 ? 30 : min;
                    token.ExpiresIn(min); //slide expiry
                }
                //retrieve user object based on APIToken
                var userRepo = WindsorConfig.Container.Resolve<IRepository<User>>();
                var user = userRepo.GetById(token.UserId);
                request.Properties.Add(Constants.CURRENT_USER, user); //add user object to request.properties
            }


            //proceed to the next step in the pipeline
            return base.SendAsync(request, cancellationToken); //TBD: send back the token.
        }

        private User getUserFromCreds(string userName, string password)
        {
            User user = null;
            var authService = WindsorConfig.Container.Resolve<AuthService>();
            try
            {
                user = authService.Login(userName, password);
            }
            catch (Exception ex)
            {
                logger.Debug(ex.ToString());
            }
            return user;
        }

    }

    static class Crypto
    {
        static readonly string key = "ca30625f-06e1-4cfd-a434-9caf7bfba048";
        private static Logger logger = LogManager.GetCurrentClassLogger();

        internal static string Encrypt<T>(T obj)
        {
            var input = JsonConvert.SerializeObject(obj);

            string encrypted = null;
            try
            {
                byte[] inputBytes = ASCIIEncoding.ASCII.GetBytes(input);

                // Create a new TripleDES service provider 
                TripleDESCryptoServiceProvider tdesProvider = new TripleDESCryptoServiceProvider();
                tdesProvider.Key = getHash(key);
                tdesProvider.Mode = CipherMode.ECB;

                encrypted = Convert.ToBase64String(tdesProvider.CreateEncryptor().TransformFinalBlock(inputBytes, 0, inputBytes.Length));
            }
            catch (Exception ex)
            {
                throw;
            }
            return encrypted;
        }



        internal static T Decrypt<T>(string encryptedString)
        {
            string decrypted = null;
            byte[] inputBytes = null;
            try
            {
                inputBytes = Convert.FromBase64String(encryptedString);
                // Create a new TripleDES service provider 
                TripleDESCryptoServiceProvider tdesProvider = new TripleDESCryptoServiceProvider();
                tdesProvider.Key = getHash(key);
                tdesProvider.Mode = CipherMode.ECB;

                decrypted = ASCIIEncoding.ASCII.GetString(tdesProvider.CreateDecryptor().TransformFinalBlock(inputBytes, 0, inputBytes.Length));
            }
            catch
            {
                throw;
            }

            return JsonConvert.DeserializeObject<T>(decrypted);
        }

        static byte[] getHash(string key)
        {
            byte[] hash = null;
            MD5CryptoServiceProvider hashmd5;

            //generate an MD5 hash from the password. 
            //a hash is a one way encryption meaning once you generate
            //the hash, you cant derive the password back from it.
            hashmd5 = new MD5CryptoServiceProvider();
            hash = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
            return hash;
        }
    }
}
