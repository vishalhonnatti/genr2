﻿using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using GenR.API.Helpers;
using GenR.Infra;
using NLog;

namespace GenR.API.Pipeline
{
    public class APIKeyHandler : DelegatingHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //logger.Debug("Entering ... " + this.GetType().Name);
            var apiHeader = request.Headers.FirstOrDefault(o => o.Key == Constants.HEADER_APIKEY);
            //Check if we have a APIKEY Header with a valid value.
            if (apiHeader.Key == null)
                return this.SendForbiddenResponse();
            else if (apiHeader.Value == null || apiHeader.Value.Count() < 1 || string.IsNullOrEmpty(apiHeader.Value.ElementAt(0)))
                return this.SendForbiddenResponse();
            else if (!validApiKey(apiHeader.Value.ElementAt(0)))
                return this.SendForbiddenResponse();
            return base.SendAsync(request, cancellationToken);
        }

        private bool validApiKey(string key)
        {
            var val = AppSettingsReader.ValueAs<string>(key, false);
            return !string.IsNullOrEmpty(val);
        }
    }
}