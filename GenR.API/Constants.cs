﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenR.API
{
    public static class Constants
    {
        public static readonly string CURRENT_USER = "CurrentUser";
        public static readonly string HEADER_APIKEY = "APIKey";
        public static readonly string HEADER_APITOKEN = "APIToken";
        public static readonly string HEADER_USERNAME = "UserName";
        public static readonly string HEADER_PASSWORD = "Password";
        public static readonly string HEADER_TOKEN_EXPIRY_IN_MINS = "Token.Expiry.In.Mins";
    }
}