﻿
namespace GenR.API.Models
{
    public class RoleDTO
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
    }
}