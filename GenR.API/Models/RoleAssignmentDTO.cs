﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenR.API.Models
{
    public class RoleAssignmentDTO
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
        public bool Allowed { get; set; }
    }
}