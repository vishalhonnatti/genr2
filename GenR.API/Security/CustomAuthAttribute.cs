﻿using System;
using System.Web;
using System.Web.Mvc;
using GenR.API.Helpers;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.Infra.Windsor;

namespace GenR.API.Security
{
    public class CustomAuthAttribute : AuthorizeAttribute
    {
        static HttpRequestBase request;
        static HttpResponseBase response;
        AuthorizationContext filterContext;


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null) { throw new ArgumentNullException("filterContext"); }
            request = filterContext.HttpContext.Request;
            response = filterContext.HttpContext.Response;
            this.filterContext = filterContext;
            //Based on code in http://aspnetwebstack.codeplex.com/SourceControl/changeset/view/000ab2c728ac#src%2fSystem.Web.Mvc%2fAuthorizeAttribute.cs
            bool skipAuth = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                                     || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

            if (skipAuth) return;
            authenticate(); //Redirects to login if authentication fails.
        }

        private void authenticate()
        {
            request = this.filterContext.HttpContext.Request;
            response = this.filterContext.HttpContext.Response;

            HttpCookie cookie = CookieHelper.GetAuthenticationCookie(request);
            if (cookie == null) { redirectToLogin(); return; }
            CookieHelper.SlideCookieExpiry(cookie, response);

            var loggedInUserId = CookieHelper.GetCookieValue(cookie);
            if (string.IsNullOrEmpty(loggedInUserId)) redirectToLogin();
            else
            {
                var repo = WindsorConfig.Container.Resolve<IRepository<User>>();
                var user = repo.GetById(loggedInUserId);
                //SessionHelper.Add(this.filterContext.HttpContext.Session, Constants.CURRENT_USER, user);
            }
        }

        private void redirectToLogin()
        {
            string loginUrl = "/account/login";
            response.Redirect(loginUrl, true);
        }
    }
}