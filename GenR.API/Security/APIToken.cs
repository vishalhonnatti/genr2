﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenR.API.Security
{
    public class APIToken
    {
        public string UserId { get; private set; }
        public DateTime ExpiresAt { get; private set; }

        public APIToken ForUser(string userId)
        {
            this.UserId = userId;
            return this;
        }

        public APIToken ExpiresIn(int min)
        {
            this.ExpiresAt = DateTime.Now.AddMinutes(min);
            return this;
        }

        public bool HasExpired()
        {
            return DateTime.Now > ExpiresAt;
        }
    }
}