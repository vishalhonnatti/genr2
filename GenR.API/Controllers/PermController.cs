﻿using System.Linq;
using System.Web.Http;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.API.Helpers;
using Newtonsoft.Json;
using GenR.API.Models;
using System.Collections.Generic;

namespace GenR.Controllers.API
{
    public class PermController : ApiController
    {
        IRepository<Perm> repo;
        public PermController(IRepository<Perm> repo)
        {
            this.repo = repo;
        }
        [Queryable(MaxTop = 20)]
        public IQueryable<PermDTO> Get()
        {
            var items = repo.GetAll();
            if (items != null && items.Count() > 0)
            {
                var itemList = new List<PermDTO>();
                foreach (var item in items)
                {
                    itemList.Add(new PermDTO() { Id = item.IdString, PermName = item.PermName });

                }

                return itemList.AsQueryable();

            }
            return null;
        }
        // GET api/permapi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/permapi
        public void Post([FromBody]string value)
        {
        }

        // PUT api/permapi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/permapi/5
        public void Delete(int id)
        {
        }
    }
}
