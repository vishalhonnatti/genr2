﻿using System.Linq;
using System.Web.Http;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.API.Helpers;
using Newtonsoft.Json;
using GenR.API.Models;
using System.Collections.Generic;

namespace GenR.Controllers.API
{
    public class RoleController : ApiController
    {
        IRepository<Roles> repo;
        IRepository<Perm> repoPerms;
        public RoleController(IRepository<Roles> repo, IRepository<Perm> repoPerms)
        {
            this.repo = repo;
            this.repoPerms = repoPerms;
        }
        [Queryable(MaxTop = 20)]
        public IQueryable<RoleDTO> Get()
        {
            var items = repo.GetAll();
            if (items != null && items.Count() > 0)
            {
                var itemList = new List<RoleDTO>();
                foreach (var item in items)
                {
                    itemList.Add(new RoleDTO() { Id = item.IdString, RoleName = item.RoleName });
                }
                return itemList.AsQueryable();
            }
            return null;
        }

        [HttpGet]
        public IList<string> Perms(string resId)
        {
            var result = new List<string>();
            var role = repo.GetById(resId);
            if (role == null || role.Permissions == null) return null;
            var permNames = from p in repoPerms.GetAll()
                            where role.Permissions.Contains(p.IdString)
                            orderby p.PermName
                            select p.PermName;

            foreach (var n in permNames)
            {
                result.Add(n);
            }
            return result;
        }
    }
}
