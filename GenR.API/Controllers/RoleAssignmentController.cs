﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.API.Models;

namespace GenR.Controllers.API
{
    public class RoleAssignmentController : ApiController
    {
        IRepository<Roles> roleRepo;
        IRepository<User> userRepo;
        public RoleAssignmentController(IRepository<User> userRepo, IRepository<Roles> roleRepo)
        {
            this.roleRepo = roleRepo;
            this.userRepo = userRepo;
        }
        [Queryable(MaxTop = 20)]
        public IQueryable<RoleAssignmentDTO> Get(string id)
        {
            var roles = roleRepo.GetAll();
            var rolesForUser = userRepo.GetById(id).Roles;

            var result = new List<RoleAssignmentDTO>();
            foreach (var role in roles)
            {
                var dto = new RoleAssignmentDTO() { Id = role.IdString, RoleName = role.RoleName };
                if (rolesForUser != null && rolesForUser.Count > 0 && rolesForUser.Contains(role.IdString))
                    dto.Allowed = true;
                result.Add(dto);
            }

            return result.AsQueryable();
        }

    }
}
