﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace GenR.API.Helpers
{
    public static class PipelineExtensions
    {
        public static Task<HttpResponseMessage> SendForbiddenResponse(this DelegatingHandler dh)
        {
            var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            var tsc = new TaskCompletionSource<HttpResponseMessage>();// Note: TaskCompletionSource creates a task that does not contain a delegate.
            tsc.SetResult(response);
            return tsc.Task;
        }
    }
}