﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Web.Configuration;
using GenR.Infra;
using System.Net.Http.Headers;

namespace GenR.API.Helpers
{
    public class CookieHelper
    {
        public static readonly string AuthCookie = "GenR";
        static readonly int AUTH_COOKIE_EXPIRES_IN_MIN = AppSettingsReader.ValueAs<int>("Auth.Cookie.Expiry.In.Mins");
        static readonly string key = "3e1a2a94-3cf8-4fb3-96dc-113fa6c0404e";

        internal static void SetAuthenticationCookie(string ckValue, HttpResponseBase response)
        {
            string cookieValue = encrypt(ckValue);
            HttpCookie cookie = new HttpCookie(AuthCookie);
            cookie.Value = cookieValue;
            cookie.Expires = DateTime.Now.AddMinutes(AUTH_COOKIE_EXPIRES_IN_MIN);
            response.Cookies.Add(cookie);
        }

        internal static HttpCookie GetAuthenticationCookie(HttpRequestBase request)
        {
            HttpCookie cookie = null;
            if (request.Cookies.Count > 0 && request.Cookies[AuthCookie] != null)
                cookie = request.Cookies[AuthCookie];

            return cookie;
        }

        internal static void RemoveAuthenticationCookie(string authCookie, HttpResponseBase response)
        {
            HttpCookie Cookie = new HttpCookie(authCookie);
            Cookie.Expires = DateTime.Now.AddMinutes(-1);
            response.Cookies.Add(Cookie);
        }

        internal static string GetCookieValue(HttpCookie cookie)
        {
            var ckValue = cookie.Value;
            return decrypt(ckValue);
        }

        internal static string GetCookieValue(CookieState cookie)
        {
            var ckValue = cookie.Value;
            return decrypt(ckValue);
        }

        internal static void SlideCookieExpiry(HttpCookie cookie, HttpResponseBase response)
        {
            cookie.Expires = DateTime.Now.AddMinutes(AUTH_COOKIE_EXPIRES_IN_MIN);
            response.Cookies.Remove(cookie.Name);
            response.Cookies.Add(cookie);
        }

        static string encrypt(string input)
        {
            string encrypted = null;
            try
            {
                byte[] inputBytes = ASCIIEncoding.ASCII.GetBytes(input);
                byte[] hash = null;
                MD5CryptoServiceProvider hashmd5;

                //generate an MD5 hash from the password. 
                //a hash is a one way encryption meaning once you generate
                //the hash, you cant derive the password back from it.
                hashmd5 = new MD5CryptoServiceProvider();
                hash = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
                hashmd5 = null;

                // Create a new TripleDES service provider 
                TripleDESCryptoServiceProvider tdesProvider = new TripleDESCryptoServiceProvider();
                tdesProvider.Key = hash;
                tdesProvider.Mode = CipherMode.ECB;

                encrypted = Convert.ToBase64String(tdesProvider.CreateEncryptor().TransformFinalBlock(inputBytes, 0, inputBytes.Length));
            }
            catch
            {
                throw;
            }
            return encrypted;
        }

        static string decrypt(string encryptedString)
        {
            string decyprted = null;
            byte[] inputBytes = null;
            try
            {
                inputBytes = Convert.FromBase64String(encryptedString);
                byte[] hash = null;
                MD5CryptoServiceProvider hashmd5;

                //generate an MD5 hash from the password. 
                //a hash is a one way encryption meaning once you generate
                //the hash, you cant derive the password back from it.
                hashmd5 = new MD5CryptoServiceProvider();
                hash = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
                hashmd5 = null;

                // Create a new TripleDES service provider 
                TripleDESCryptoServiceProvider tdesProvider = new TripleDESCryptoServiceProvider();
                tdesProvider.Key = hash;
                tdesProvider.Mode = CipherMode.ECB;

                decyprted = ASCIIEncoding.ASCII.GetString(tdesProvider.CreateDecryptor().TransformFinalBlock(inputBytes, 0, inputBytes.Length));
            }
            catch
            {
                throw;
            }
            return decyprted;
        }
    }
}