﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using GenR.API.Pipeline;

namespace GenR.API
{
    public static class WebApiConfig
    {
        public static void RegisterRoutes(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
               name: "API Action",
               routeTemplate: "api/{controller}/{action}/{resId}",
               defaults: new { resId = RouteParameter.Optional }
           );

        }
        public static void ConfigureMessageHandlers(HttpConfiguration config)
        {
            config.MessageHandlers.Add(new APIKeyHandler());
            config.MessageHandlers.Add(new APITokenHandler());

        }
    }
}
