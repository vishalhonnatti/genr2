﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GenR.Domain
{
    internal static class CustomValidator
    {
        public static IEnumerable<ErrorInfo> GetErrors(object instance)
        {
            return from prop in TypeDescriptor.GetProperties(instance).Cast<PropertyDescriptor>()
                   from attribute in prop.Attributes.OfType<ValidationAttribute>()
                   where !attribute.IsValid(prop.GetValue(instance))
                   select new ErrorInfo(prop.Name, attribute.FormatErrorMessage(string.Empty));
        }
    }

    public class ErrorInfo
    {
        public string Property { get; set; }
        public string Message { get; set; }
        public ErrorInfo(string prop, string message)
        {
            this.Property = prop;
            this.Message = message;
        }
    }
}
