﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace GenR.Domain.Security
{
    public enum UserStatusEnum { Active, Disabled }
    public class User : Entity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailValidation(ErrorMessage = "Invalid email.")]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public string Salt { get; set; }
        public UserStatusEnum Status { get; set; }
        public List<string> Roles { get; set; }
        public UserSettings Settings { get; set; }
        public User()
            : base("Users")
        {
            this.Status = UserStatusEnum.Active;
            this.Settings = new UserSettings();
        }

        public IEnumerable<ErrorInfo> Validate()
        {
            return CustomValidator.GetErrors(this);
        }
    }
}
