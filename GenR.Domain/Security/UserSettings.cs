﻿namespace GenR.Domain.Security
{
    public class UserSettings
    {
        public int PageSize { get; set; }
        public UserSettings()
        {
            this.PageSize = 10;//default page size
        }
    }
}
