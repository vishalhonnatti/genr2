﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace GenR.Domain.Security
{
    public class Perm : Entity
    {
        [Required]
        public string PermName { get; set; }
                public Perm()
            : base("Perm")
        {

        }

        public IEnumerable<ErrorInfo> Validate()
        {
            return CustomValidator.GetErrors(this);
        }
    }
}