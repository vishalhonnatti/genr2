﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace GenR.Domain.Security
{
    public class Roles : Entity
    {
        [Required]
        public string RoleName { get; set; }
        public List<string> Permissions { get; set; }

        public Roles() : base("Roles") { Permissions = new List<string>(); }

        public IEnumerable<ErrorInfo> Validate()
        {
            return CustomValidator.GetErrors(this);
        }
    }
}