﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace GenR.Domain
{
    public abstract class Entity
    {
        [BsonId]
        public ObjectId Id { get; set; }

        public String IdString
        {
            get { return Id.ToString(); }
            set { if (!string.IsNullOrEmpty(value)) { Id = new ObjectId(value); }; }
        }

        [BsonIgnore]
        public string CollectionName { get; private set; }

        protected Entity(string collectionName)
        {
            CollectionName = collectionName;
        }
    }
}