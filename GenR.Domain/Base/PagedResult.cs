﻿using System.Collections.Generic;

namespace GenR.Domain
{
    public class PagedResult<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public long TotalRecords { get; set; }
        public long TotalPages
        {
            get
            {
                return TotalRecords / PageSize + ((TotalRecords % PageSize > 0) ? 1 : 0);
            }
        }
        public IEnumerable<T> PageData { get; set; }
    }
}