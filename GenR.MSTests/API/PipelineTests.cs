﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using NLog;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using Castle.Windsor;
using GenR.Infra.Windsor;
using System.Configuration;
using GenR.API.Security;



namespace GenR.MSTests.API
{
    [TestClass]
    public class PipelineTests
    {
        public static IWindsorContainer Container;
        [TestClass]
        public class APIKeyHandler
        {
           
            private string apiURL = "http://localhost:4949/api/";
            HttpClient client = new HttpClient();
            
            private static Logger logger = LogManager.GetCurrentClassLogger();
            private static string APIKEY = "7da4389cbe28";
          
            [TestInitialize]
            public void Init()
            {
                
                WindsorConfig.Configure();
                Container = WindsorConfig.Container;
                client.BaseAddress = new Uri(apiURL);
            }

            [TestMethod]
            [TestCategory("APIKeyHandler")]
            public void cannot_request_without_apikey()
            {
                client.DefaultRequestHeaders.Remove("APIKey");
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.Forbidden);
            }
            [TestMethod]
            public void cannot_request_with_empty_apikey()
            {
                client.DefaultRequestHeaders.Remove("APIKey");
                client.DefaultRequestHeaders.Add("APIKey", "");
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.Forbidden);
            }
            [TestMethod]
            public void cannot_request_with_invalid_apikey()
            {
                client.DefaultRequestHeaders.Add("APIKey", "InvalidKey");
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.Forbidden);
            }
        }

        [TestClass]
        public class APITokenHandler
        {
            private string apiURL = "http://localhost:4949/api/";
            HttpClient client = new HttpClient();
            private static Logger logger = LogManager.GetCurrentClassLogger();
            private static string APIKEY = "7da4389cbe28";

            [TestInitialize]
            public void Init()
            {
                client.BaseAddress = new Uri(apiURL);
            }
            [TestMethod]
            public void can_authenticate_with_valid_credentials()
            {
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "mayuri");
                client.DefaultRequestHeaders.Add("Password", "winter123");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
                IEnumerable<string> values;
                var apiTokenHeader = response.Headers.TryGetValues("APIToken", out values);
                Assert.IsTrue(values.ToList().Count == 1);
            }
            [TestMethod]
            public void authentication_fails_with_empty_userName()
            {
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "");
                client.DefaultRequestHeaders.Add("Password", "winter123");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.Forbidden);
            }
            [TestMethod]
            public void authentication_fails_with_empty_password()
            {
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "mayuri");
                client.DefaultRequestHeaders.Add("Password", "");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.Forbidden);
            }
            [TestMethod]
            public void authentication_fails_with_invalid_credentials()
            {
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "sgmayuri");
                client.DefaultRequestHeaders.Add("Password", "123");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.Forbidden);
            }

            [TestMethod]
            public void cannot_request_with_no_token()
            {
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.IsTrue(response.StatusCode == HttpStatusCode.Forbidden);
            }
            [TestMethod]
            public void cannot_request_with_empty_token()
            {
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("APIToken", string.Empty);
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.IsTrue(response.StatusCode == HttpStatusCode.Forbidden);
            }
            [TestMethod]
            public void cannot_request_with_invalid_token()
            {
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("APIToken", "abcdef");
                var task = client.GetAsync("user");
                var response = task.Result;
                Assert.IsTrue(response.StatusCode == HttpStatusCode.Forbidden);
            }

            [TestMethod]
            public void can_request_with_valid_token()
            {
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "mayuri");
                client.DefaultRequestHeaders.Add("Password", "winter123");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
                IEnumerable<string> values;
                var apiTokenHeader = response.Headers.TryGetValues("APIToken", out values);
                Assert.IsTrue(values.ToList().Count == 1);
                Console.WriteLine(values.ElementAt(0));
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("APIToken", values.ElementAt(0));
                task = client.GetAsync("user");
                Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
                Console.WriteLine(task.Result.Content.ReadAsStringAsync().Result);
            }

            [TestMethod]
            public void cannot_request_when_token_is_expired()
            {
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("UserName", "mayuri");
                var task = client.GetAsync("authenticate");
                var response = task.Result;
                Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
                IEnumerable<string> values;
                 var apiTokenHeader = response.Headers.TryGetValues("APIToken", out values);
                 Console.WriteLine(values.ElementAt(0));
                client.DefaultRequestHeaders.Add("APIKey", APIKEY);
                client.DefaultRequestHeaders.Add("APIToken", values.ElementAt(0));
                 task = client.GetAsync("user");
                  var token = WindsorConfig.Container.Resolve<APIToken>();
                if(token.HasExpired())
                Assert.IsTrue(response.StatusCode == HttpStatusCode.Forbidden);
                 }
        }


    }
}
