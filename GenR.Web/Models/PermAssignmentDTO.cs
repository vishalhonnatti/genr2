﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenR.Web.Models
{
    public class PermAssignmentDTO
    {
        public string Id { get; set; }
        public string PermName { get; set; }
        public bool Allowed { get; set; }
    }
}