﻿using GenR.Domain.Security;

namespace GenR.Web.Models
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
        public UserSettings Settings { get; set; }
    }
}