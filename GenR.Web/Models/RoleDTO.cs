﻿
namespace GenR.Web.Models
{
    public class RoleDTO
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
    }
}