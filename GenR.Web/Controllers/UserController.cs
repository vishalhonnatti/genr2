﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.Services;
using GenR.Web.Models;
using System.Net.Http;
using System;

namespace GenR.Web.Controllers
{

    public class UserController : Controller
    {

        IRepository<User> repo;
        AuthService authService;
        UserService userService;

        public UserController(IRepository<User> repo, AuthService authService, UserService userService)
        {
            this.repo = repo;
            this.authService = authService;
            this.userService = userService;

        }


        public ActionResult Index()
        {
            var items = repo.GetAll();
            //Create a list of anon object with the fields that we require & then clear the list.
            var itemList = new List<UserDTO>();
            if (items != null && items.Count() > 0)
            {
                itemList.Clear();
                //Now fill the list 
                foreach (var item in items)
                {
                    itemList.Add(new UserDTO() { Id = item.IdString, Name = item.Name, Email = item.Email, Status = item.Status.ToString(), Password = null });

                }
            }
            return View(itemList);
        }

        [HttpPost]
        public void Enable(string id)
        {
            var user = repo.GetById(id);
            if (user == null) return;
            user.Status = UserStatusEnum.Active;
            repo.Update(user);
        }

        [HttpPost]
        public void Disable(string id)
        {
            var user = repo.GetById(id);
            if (user == null) return;
            user.Status = UserStatusEnum.Disabled;
            repo.Update(user);
        }
        [HttpPost]
        public MvcHtmlString Reset(string id)
        {
            var newPassword = authService.AdminReset(id);
            return new MvcHtmlString(newPassword.Trim());
        }

        [HttpPost]
        public void Create(string Name, string Email)
        {
            string pwd = string.Empty;
            var newUser = userService.CreateUser(Name, Email, out pwd);
        }


        [HttpPost]
        public void Edit(string id, string Name, string Email)
        {
            var user = repo.GetById(id);
            if (user == null) return;
            user.Name = Name;
            user.Email = Email;
            repo.Update(user);
        }

        public void setRole(string id, List<string> roles)
        {
            var user = repo.GetById(id);
            if (user == null) return;
            user.Roles = roles;
            repo.Update(user);
        }
        public void userSetting(string id)
        {
            var user = repo.GetById(id);
            if (user.Settings == null)
                return;
            user.Settings.PageSize.Equals(20);
        }
    }
}
