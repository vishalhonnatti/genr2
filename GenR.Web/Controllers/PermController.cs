﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.Services;
using GenR.Web.Models;
using System.Net.Http;
using System;

namespace GenR.Web.Controllers
{
    public class PermController : Controller
    {
        IRepository<Perm> repo;
        AuthService authService;
        PermService permService;

        public PermController(IRepository<Perm> repo, AuthService authService, PermService permService)
        {
            this.repo = repo;
            this.authService = authService;
            this.permService = permService;

        }


        public ActionResult Index()
        {
            var items = repo.GetAll();
            //Create a list of anon object with the fields that we require & then clear the list.
            var itemList = new List<PermDTO>();
            if (items != null && items.Count() > 0)
            {
                itemList.Clear();
                //Now fill the list 
                foreach (var item in items)
                {
                    itemList.Add(new PermDTO() {Id=item.IdString, PermName = item.PermName });

                }
            }
            return View(itemList);
        }


        [HttpPost]
        public void Create(string PermName)
        {
            var newPerm = permService.CreatePerm(PermName);
        }

        [HttpPost]
        public void Edit(string id, string PermName)
        {
            var perm = repo.GetById(id);
            if (perm == null) return;
            perm.PermName = PermName;
             repo.Update(perm);
        }

    }
}

