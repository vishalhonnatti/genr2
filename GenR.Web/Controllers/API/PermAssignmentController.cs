﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.Web.Models;

namespace GenR.Web.Controllers.API
{
    public class PermAssignmentController : ApiController
    {
        IRepository<Roles> roleRepo;
        IRepository<Perm> permRepo;
        public PermAssignmentController(IRepository<Perm> permRepo, IRepository<Roles> roleRepo)
        {
            this.roleRepo = roleRepo;
            this.permRepo = permRepo;
        }
        [Queryable(MaxTop = 20)]
        public IQueryable<PermAssignmentDTO> Get(string id)
        {
            var perms = permRepo.GetAll();
            var permsForRole = roleRepo.GetById(id).Permissions;

            var result = new List<PermAssignmentDTO>();
            foreach (var perm in perms)
            {
                var dto = new PermAssignmentDTO() { Id = perm.IdString, PermName = perm.PermName };
                if (permsForRole != null && permsForRole.Count > 0 && permsForRole.Contains(perm.IdString))
                    dto.Allowed = true;
                result.Add(dto);
            }

            return result.AsQueryable();
        }
        
    }
}
