﻿using System.Linq;
using System.Web.Http;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.Web.Helpers;
using Newtonsoft.Json;
using GenR.Web.Models;
using System.Collections.Generic;

namespace GenR.Web.Controllers.API
{
    public class UserController : ApiController
    {
        IRepository<User> repo;
        IRepository<Roles> repoRoles;
        public UserController(IRepository<User> repo, IRepository<Roles> repoRoles)
        {
            this.repo = repo;
            this.repoRoles = repoRoles;
        }
        [Queryable(MaxTop = 30)]
        public IQueryable<UserDTO> Get()
        {
            var items = repo.GetAll();
            if (items != null && items.Count() > 0)
            {
                var itemList = new List<UserDTO>();
                foreach (var item in items)
                {
                    itemList.Add(new UserDTO() { Id = item.IdString, Name = item.Name, Email = item.Email, Status = item.Status.ToString(), Password = string.Empty,Settings = item.Settings });

                }

                return itemList.AsQueryable();

            }
            return null;
        }
        [HttpGet]
        public IList<string> Roles(string resId)
        {
            var result = new List<string>();
            var user = repo.GetById(resId);
            if (user == null || user.Roles == null) return null;
            var roleNames = from r in repoRoles.GetAll()
                            where user.Roles.Contains(r.IdString)
                            orderby r.RoleName
                            select r.RoleName;

            foreach (var n in roleNames)
            {
                result.Add(n);
            }
            return result;
        }
        
    }
}