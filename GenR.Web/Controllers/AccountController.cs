﻿using System.Web.Mvc;
using GenR.Web.Helpers;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.Services;
using GenR.Web.App_Start;
using Global = GenR.Web.MvcApplication;
using System;

namespace GenR.Web.Controllers
{
    public class AccountController : Controller
    {
        AuthService authService;
        public AccountController(AuthService authService)
        {
            this.authService = authService;
        }

        [AllowAnonymous]
        public ActionResult Login() { return View(); }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            //authenticate
            User user = null;
            try
            {
                user = authService.Login(userName, password);
            }
            catch (Exception ex)
            {
                //log error message.
            }
            //succeeds
            if (user != null)
            {
                CookieHelper.SetAuthenticationCookie(user.IdString, Response);
                return RedirectToAction("index", "home");
            }
            //fails
            else
            {
                TempData["loginFailed"] = "Login failed.";
                return RedirectToAction("login", "account");
            }
        }
        public ActionResult Logout()
        {
            CookieHelper.RemoveAuthenticationCookie("GenR", Response);
            return RedirectToAction("Login");
        }
        public ActionResult Profile()
        {
            return View();
        }
        public ActionResult Preferences()
        {
            return View();
        }
    }
}
