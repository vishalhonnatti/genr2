﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GenR.Domain;
using GenR.Domain.Security;
using GenR.Services;
using GenR.Web.Models;
using System.Net.Http;
using System;

namespace GenR.Web.Controllers
{

    public class RoleController : Controller
    {

        IRepository<Roles> repo;
        IRepository<Perm> perm;
        AuthService authService;
        AccountService roleService;

        public RoleController(IRepository<Roles> repo, IRepository<Perm> perm, AuthService authService, AccountService roleService)
        {
            this.repo = repo;
            this.perm = perm;
            this.authService = authService;
            this.roleService = roleService;
        }

        public ActionResult Index()
        {
            var items = repo.GetAll();
            //Create a list of anon object with the fields that we require & then clear the list.
            var itemList = new List<RoleDTO>();
            if (items != null && items.Count() > 0)
            {
                itemList.Clear();
                //Now fill the list 
                foreach (var item in items)
                {
                    itemList.Add(new RoleDTO() { Id = item.IdString, RoleName = item.RoleName });

                }
            }
            return View(itemList);
        }


        [HttpPost]
        public void Create(string RoleName)
        {
            var newRole = roleService.CreateRole(RoleName);
        }

        [HttpPost]
        public void Edit(string id, string RoleName)
        {
            var role = repo.GetById(id);
            if (role == null) return;
            role.RoleName = RoleName;
            repo.Update(role);
        }

        public void setpermission(string id, List<string> permission)
        {
            var role = repo.GetById(id);
            if (role == null) return;
            role.Permissions = permission;
            repo.Update(role);
        }
    }
}

