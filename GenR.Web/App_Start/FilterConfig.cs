﻿using System.Web;
using System.Web.Mvc;
using GenR.Web.Security;

namespace GenR.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomAuthAttribute());
        }
    }
}