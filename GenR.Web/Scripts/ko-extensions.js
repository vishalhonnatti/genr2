﻿//wrapper to an observable that requires accept/cancel
ko.protectedObservable = function (initialValue) {
    //private variables
    var _actualValue = ko.observable(initialValue),
        _tempValue = initialValue;

    //computed observable that we will return
    var result = ko.computed({
        //always return the actual value
        read: function () {
            return _actualValue();
        },
        //stored in a temporary spot until commit
        write: function (newValue) {
            _tempValue = newValue;
        }
    });

    //if different, commit temp value
    result.commit = function () {
        if (_tempValue !== _actualValue()) {
            _actualValue(_tempValue);
        }
    };

    //force subscribers to take original
    result.reset = function () {
        _actualValue.valueHasMutated();
        _tempValue = _actualValue();   //reset temp value
    };

    return result;
};

/**
* RETURN key binding for Knockout.js
*/
ko.bindingHandlers.returnKey = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var allBindings = allBindingsAccessor();
        ko.utils.registerEventHandler(element, 'keydown', function (evt) {
                      if (evt.keyCode === 13) {
                evt.preventDefault();
                evt.target.blur();
                allBindings.returnKey.call(viewModel);
                //valueAccessor().call(viewModel);
            }
        });
    }
};

/*ko.bindingHandlers.returnKey = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var allBindings = allBindingsAccessor();
       $(element).keypress(function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);
           if (keyCode === 13) {
               allBindings.returnKey.call(viewModel);
               return false;
           }
            return true;
       });
    }
};*/