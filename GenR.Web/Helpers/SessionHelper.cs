﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenR.Web.Helpers
{
    public class SessionHelper
    {
        public static void Add(HttpSessionStateBase session, string key, object value)
        {
            session.Add(key, value);
        }
        public static T Get<T>(HttpSessionStateBase session, string key)
        {
            return (T)session[key];
        }
    }
}