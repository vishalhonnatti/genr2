﻿using System;
using System.Linq;
using GenR.Domain;
using GenR.Domain.Security;
using Newtonsoft.Json;
namespace GenR.Services
{
    public class AccountService : IService
    {
        IRepository<Roles> repo;
               public AccountService(IRepository<Roles> repoRole)
        {
            this.repo = repoRole;
           
        }
        public Roles CreateRole(string roleName)
        {
            //Do sanity checks for context and passed in parameters
          
            if (repo == null)
                throw new Exception("User repo cannot be null.");
            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentNullException("roleName", "Value cannot be null.");
            
            var role = new Roles() { RoleName = roleName};
            var error = role.Validate();
            if (error.Count() > 0)
                throw new Exception(JsonConvert.SerializeObject(error));

            repo.Add(role);

            return role;
        }
       
    }
}