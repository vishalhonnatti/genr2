﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace GenR.Services
{
    public class SaltedHash
    {
        public static string Generate(string input, string salt)
        {
            HashAlgorithm algorithm = new SHA256Managed();

            var inputByteArray = Encoding.UTF8.GetBytes(input);
            var saltByteArray = Encoding.UTF8.GetBytes(salt);

            byte[] plainTextWithSaltBytes =
              new byte[inputByteArray.Length + saltByteArray.Length];

            for (int i = 0; i < inputByteArray.Length; i++)
            {
                plainTextWithSaltBytes[i] = inputByteArray[i];
            }
            for (int i = 0; i < saltByteArray.Length; i++)
            {
                plainTextWithSaltBytes[inputByteArray.Length + i] = saltByteArray[i];
            }
            var hash = algorithm.ComputeHash(plainTextWithSaltBytes);
            return Convert.ToBase64String(hash);
        }
    }
}
