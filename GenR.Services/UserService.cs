﻿using System;
using System.Linq;
using GenR.Domain;
using GenR.Domain.Security;
using Newtonsoft.Json;

namespace GenR.Services
{
    public class UserService : IService
    {
        IRepository<User> repo;
        AuthService authService;
        public UserService(IRepository<User> repoUser, AuthService authService)
        {
            this.repo = repoUser;
            this.authService = authService;
        }
        public User CreateUser(string userName, string email, out string pwd)
        {
            //Do sanity checks for context and passed in parameters
            if (repo == null)
                throw new Exception("User repo cannot be null.");
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException("userName", "Value cannot be null.");
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException("email", "Value cannot be null.");

            //delegate non-core functionality
            pwd = authService.GeneratePassword();
            var salt = authService.GenerateSalt();

            var user = new User() { Name = userName, Password = SaltedHash.Generate(pwd, salt), Email = email.Trim().ToLower(), Salt = salt };
            var error = user.Validate();
            if (error.Count() > 0)
                throw new Exception(JsonConvert.SerializeObject(error));

            var userExists = repo.GetSingle(o => o.Email.Equals(email)) != null;
            if (userExists)
                throw new Exception(string.Format("User with email {0} already exists.", email.Trim().ToLower()));

            repo.Add(user);

            return user;
        }
    }
}
