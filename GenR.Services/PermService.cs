﻿using System;
using System.Linq;
using GenR.Domain;
using GenR.Domain.Security;
using Newtonsoft.Json;
namespace GenR.Services
{
    public class PermService : IService
    {
        IRepository<Perm> repo;
        public PermService(IRepository<Perm> repoPerm)
        {
            this.repo = repoPerm;

        }
        public Perm CreatePerm(string permName)
        {
            //Do sanity checks for context and passed in parameters
            if (repo == null)
                throw new Exception("User repo cannot be null.");
            if (string.IsNullOrWhiteSpace(permName))
                throw new ArgumentNullException("permName", "Value cannot be null.");
              var perm = new Perm() { PermName = permName };
            var error = perm.Validate();
            if (error.Count() > 0)
                throw new Exception(JsonConvert.SerializeObject(error));

            repo.Add(perm);

            return perm;
        }
    }
}