﻿using System;
using System.Linq;
using GenR.Domain;
using GenR.Domain.Security;
using Newtonsoft.Json;
using NLog;

namespace GenR.Services
{
    /// <summary>
    /// Authentication & Authorization service
    /// </summary>
    public class AuthService : IService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        IRepository<User> repo;
        public AuthService(IRepository<User> repoUser)
        {
            this.repo = repoUser;
        }

        public User Login(string userName, string password)
        {
            //Do sanity checks for context and passed in parameters
            if (repo == null)
                throw new Exception("User repo cannot be null.");
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException("userName", "Value cannot be null.");
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException("password", "Value cannot be null.");

            var user = repo.GetSingle(o => o.Name == userName);
            if (user == null)
                throw new Exception("User record not found for " + userName);
            if (user.Status == UserStatusEnum.Disabled)
                throw new Exception(string.Format("User {0} is disabled.", user.Name));

            var hash = SaltedHash.Generate(password, user.Salt);
            if (user.Password.Equals(hash))
                return user;
            else
                throw new Exception(string.Format("Login Failed", user.Name));

        }
        public void DisableUser(string id)
        {
            //Do sanity checks for context and passed in parameters
            if (repo == null)
                throw new Exception("User repo cannot be null.");
            var user = repo.GetById(id);
            if (user == null)
                throw new Exception("User record not found with id: " + id);
            user.Status = UserStatusEnum.Disabled;
            repo.Update(user);
        }
        public void EnableUser(string id)
        {
            //Do sanity checks for context and passed in parameters
            if (repo == null)
                throw new Exception("User repo cannot be null.");
            var user = repo.GetById(id);
            if (user == null)
                throw new Exception("User record not found with id: " + id);
            user.Status = UserStatusEnum.Active;
            repo.Update(user);
        }
        /// <summary>
        /// A logged in user resets the password.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        public void ResetPasswordByUser(string email)
        {
            if (repo == null)
                throw new Exception("User repo cannot be null.");

            var user = repo.GetSingle(o => o.Email.Equals(email.Trim().ToLower()));
            if (user == null) throw new Exception("User record not found with email: " + email);
            //Verify user is active
            if (user.Status == UserStatusEnum.Disabled) throw new Exception(string.Format("User ({0}) {1} is disabled", user.IdString, user.Email));

            var pw = GeneratePassword();
            var newSalt = GenerateSalt();
            var newHashedPassword = SaltedHash.Generate(pw, newSalt);
            user.Salt = newSalt;
            user.Password = newHashedPassword;
            var errors = user.Validate();
            if (errors.Count() > 0)
                throw new Exception(JsonConvert.SerializeObject(errors));
            logger.Info("Password reset successful for user: {0} with new password: {1}", user.Email, pw);
            repo.Update(user);

        }
        /// <summary>
        /// A logged in user changes the password
        /// </summary>
        /// <param name="id"></param>
        /// <param name="email"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <param name="userName"></param>
        public void ChangePasswordByUser(string id, string oldPassword, string newPassword) //Take out email & username
        {
            if (repo == null)
                throw new Exception("User repo cannot be null.");

            User user = repo.GetById(id);
            if (user == null) throw new Exception("User record not found with id: " + id);
            if (user.Status == UserStatusEnum.Disabled) throw new Exception(string.Format("User ({0}) {1} is disabled", user.IdString, user.Email));

            var oldHashedPassword = SaltedHash.Generate(oldPassword, user.Salt);
            if (user.Password != oldHashedPassword) throw new Exception(Constants.MSG_PASSWORD_MISMATCH);

            var newSalt = GenerateSalt();
            var newHashedPassword = SaltedHash.Generate(newPassword, newSalt);
            user.Salt = newSalt;
            user.Password = newHashedPassword;
            var errors = user.Validate();
            if (errors.Count() > 0)
                throw new Exception(JsonConvert.SerializeObject(errors));

            var userexists = repo.GetSingle(o => o.Email.Equals(user.Email)) != null;

            if (!userexists)
                throw new Exception(string.Format("User does not exist ", user.Email.Trim().ToLower()));
            //Reorganize => move purely internal functions towards top.
            if (oldPassword == newPassword)
                throw new Exception("New Password cannot be same as old Password");
            if (string.IsNullOrWhiteSpace(newPassword))
                throw new ArgumentNullException("newPassword", "Value cannot be null.");
            if (string.Equals(user.Name.ToLower(), newPassword))
                throw new Exception("Cannot be same as Username");
            if (newPassword.Length <= 3)
                throw new Exception("Minimum Password length should be 3 ");
            repo.Update(user);
        }
        /// <summary>
        /// Admin resets the password
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newPassword"></param>
        /// <param name="email"></param>
        public string AdminReset(string id)//take out email
        {
            if (repo == null)
                throw new Exception("User repo cannot be null.");
            var user = repo.GetById(id);
            if (user == null)
                throw new Exception("User record not found with id: " + id);
            if (user.Status == UserStatusEnum.Disabled)
                throw new Exception(string.Format("User ({0}) {1} is disabled", user.IdString, user.Email)); //standardize the message

            var pw = GeneratePassword();
            var newSalt = GenerateSalt();
            var newHashedPassword = SaltedHash.Generate(pw, newSalt);
            user.Salt = newSalt;
            user.Password = newHashedPassword;
            var errors = user.Validate();
            if (errors.Count() > 0)
                throw new Exception(JsonConvert.SerializeObject(errors));
            logger.Info("User {0} password reset with password {1}", user.Name, pw);
            repo.Update(user);
            //todo: improvise so that the password is emailed to the user and the admin doesn't have to see the password.
            return pw; //return the generated password so that admin can hand over to the user.
        }

        public string GeneratePassword()
        {
            var guid = Guid.NewGuid().ToString();
            return guid.Substring(0, 8);
        }

        public string GenerateSalt()
        {
            var guid = Guid.NewGuid().ToString();
            return guid.Substring(24, 12);
        }

    }
}
